#!/bin/bash

function info {
    printf "\033[0;36m===> \033[0;33m${1}\033[0m\n"
}

if [ $SETTINGS_ENABLED -eq 1 ]; then

    info "------------> Idę spać na $SLEEP_TIME sek. :) <-------------"
    info "----------------> Czekam na MySQL <-----------------"
    sleep $SLEEP_TIME
    info "-------------------> Wstałem :) <-------------------"

    info "-------> Ustawiam base URL i base link URL (secure, unsecure) <------"

    unsecureUrl=$(mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -N -e 'SELECT value FROM `core_config_data` WHERE path = "web/unsecure/base_url";')
    secureUrl=$(mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -N -e 'SELECT value FROM `core_config_data` WHERE path = "web/secure/base_url";')

    if [ "$unsecureUrl" = "$PAGE_URL_UNSECURE" ]; then
        info "Unsecure URL w bazie zgodny z wymaganym przez użytkownika."
        info $PAGE_URL_UNSECURE
    else
        info "Ustawiony nowy unsecure URL"
        info $PAGE_URL_UNSECURE

        mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$PAGE_URL_UNSECURE'" WHERE path = "web/unsecure/base_url";'
        mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$PAGE_URL_UNSECURE'" WHERE path = "web/unsecure/base_link_url";'
    fi

    if [ "$secureUrl" = "$PAGE_URL_SECURE" ]; then
        info "Secure URL w bazie zgodny z wymaganym przez użytkownika."
        info $PAGE_URL_SECURE
    else
        info "Ustawiony nowy secure URL"
        info $PAGE_URL_SECURE

        mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$PAGE_URL_SECURE'" WHERE path = "web/secure/base_url";'
        mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$PAGE_URL_SECURE'" WHERE path = "web/secure/base_link_url";'
    fi

    info "-----> Ustawiam email'e w konfiguracji sklepu <-----"

    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "trans_email/ident_general/email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "trans_email/ident_sales/email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "trans_email/ident_support/email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "trans_email/ident_custom1/email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "trans_email/ident_custom2/email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/order/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/order_comment/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/invoice/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/invoice_comment/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/shipment/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/shipment_comment/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/creditmemo/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "sales_email/creditmemo_comment/copy_to";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "contacts/email/recipient_email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "'$DEV_EMAIL'" WHERE path = "smtppro/general/googleapps_email";'
    mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE -e 'UPDATE `core_config_data` SET value = "google" WHERE path = "smtppro/general/option";'

    info "----------------------------------------------------"
fi

php-fpm
