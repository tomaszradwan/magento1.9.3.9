# Docker for Magento v.1.9.3.9

## PHP v.5.6, Nginx, Redis, MySQL v.5.6, PhpMyAdmin
#

## Rozpocznij pracę z projektem
* przejdź do folderu ./www i sklonuj repozytorium ```git clone https://github.com/tomaszradwan/magento.git ```
* w folderze z plikiem docker-compose.yml wykonaj w terminalu polecenie ./up.sh (plik zawiera komendę docker-compose up)
* wyczyść cache w kontenerze magento_redis
* aby utworzyć konto administratora w Magento przejdź do PhpMyAdmin i wykonaj poniższą SQL'kę zmieniając pola na właściwe dla Ciebie:
```
<admin_password>
<admin_firstname>
<admin_lastname>
<admin_email>
<admin_username>
```
```
LOCK TABLES `admin_role` WRITE , `admin_user` WRITE;
 
SET @SALT = "ls";
SET @PASS = CONCAT(MD5(CONCAT( @SALT , "<admin_password>") ), CONCAT(":", @SALT ));
SELECT @EXTRA := MAX(extra) FROM admin_user WHERE extra IS NOT NULL;
 
INSERT INTO `admin_user` (firstname,lastname,email,username,password,created,lognum,reload_acl_flag,is_active,extra,rp_token_created_at)
VALUES ('<admin_firstname>','<admin_lastname>','<admin_email>','admin_username>',@PASS,NOW(),0,0,1,@EXTRA,NOW());
 
INSERT INTO `admin_role` (parent_id,tree_level,sort_order,role_type,user_id,role_name) 
VALUES (1,2,0,'U',(SELECT user_id FROM admin_user WHERE username = 'admin_username>'),'<admin_firstname>');
 
UNLOCK TABLES;
```


## GIT
Wyłączenie śledzenia pliku .env.
``` git update-index --assume-unchanged opt/.env ```
https://stackoverflow.com/questions/936249/how-to-stop-tracking-and-ignore-changes-to-a-file-in-git


## REDIS - pomocne komendy/linki

Wykonać w kontenerze redis:
* redis-cli flushall - czyszczenie pamięci cache
* redis-cli --scan - wyświetlenie wszystkich kluczy w cache'u

https://support.hypernode.com/knowledgebase/configure-redis-magento1/
https://redis.io/commands
https://czterytygodnie.pl/wprowadzenie-do-redis-a/


## Magento 1.9 - moduł do wysyłki e-mail'i

https://github.com/aschroder/Magento-SMTP-Pro-Email-Extension


## MySQL
* dump bazy: ``` mysqldump -u user -phaslo nazwa_bazy > kopia_bazy.sql ```
http://designconcept.webdev20.pl/articles/mysqldump-kopia-zapasowa-bazy-danych


## Xdebug dla PhpStorm

https://intellij-support.jetbrains.com/hc/en-us/community/posts/360000149204-phpStorm-xDebug-Docker-not-working-in-Windows-10


## Pomocne linki

https://stackoverflow.com/questions/15423500/nginx-showing-blank-php-pages
https://www.webinse.com/blog/cant-login-magento-admin-panel/
https://webnoo.com/support/knowledge/create-new-admin-user-magento-via-phpmyadmin-mysql-command/